CREATE SCHEMA clase7;

CREATE TABLE clase7.empresa
(
    id             bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fecha_creacion datetime,
    razon_social   varchar(255),
    representante  varchar(255),
    ruc            varchar(255)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE clase7.persona
(
    id              bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    direccion       varchar(255),
    email           varchar(255),
    nombre          varchar(255),
    telefono        varchar(255),
    empresa_id      bigint,
    id_persona      bigint NOT NULL,
    persona_list_id bigint
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE clase7.rol
(
    id     bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(255)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE clase7.usuario
(
    id          bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    password    varchar(255),
    username    varchar(255),
    empleado_id bigint
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE clase7.empresa_persona_list
(
    empresa_id      bigint NOT NULL,
    persona_list_id bigint NOT NULL,
    CONSTRAINT `UK_lrgfun09kwqd60aa4grrm42xl` UNIQUE (persona_list_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE INDEX `FKrvl5v1dpvx13a46b6wxo0qx0j` ON clase7.persona (empresa_id);

CREATE INDEX `FKgot3jq0eng9lvyja174qoe1a5` ON clase7.persona (persona_list_id);

CREATE INDEX `FK44o5rsj3cs2hsw2gkg3056ivm` ON clase7.usuario (empleado_id);

CREATE INDEX `FK3hkcdmd3tnvqg683r4cf0mgpn` ON clase7.empresa_persona_list (empresa_id);

ALTER TABLE clase7.empresa_persona_list
    ADD CONSTRAINT `FK3hkcdmd3tnvqg683r4cf0mgpn` FOREIGN KEY (empresa_id) REFERENCES clase7.empresa (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE clase7.empresa_persona_list
    ADD CONSTRAINT `FK90f46adsww2o9oq1gf4100wdd` FOREIGN KEY (persona_list_id) REFERENCES clase7.persona (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE clase7.persona
    ADD CONSTRAINT `FKgot3jq0eng9lvyja174qoe1a5` FOREIGN KEY (persona_list_id) REFERENCES clase7.empresa (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE clase7.persona
    ADD CONSTRAINT `FKrvl5v1dpvx13a46b6wxo0qx0j` FOREIGN KEY (empresa_id) REFERENCES clase7.empresa (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE clase7.usuario
    ADD CONSTRAINT `FK44o5rsj3cs2hsw2gkg3056ivm` FOREIGN KEY (empleado_id) REFERENCES clase7.persona (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

INSERT INTO clase7.empresa(id, fecha_creacion, razon_social, representante, ruc)
VALUES (1, '1994-12-31 02.00.00 p.m.', 'oswaldo', 'silviopd222', '23aaasd2424');
INSERT INTO clase7.empresa(id, fecha_creacion, razon_social, representante, ruc)
VALUES (2, null, 'oswaldo', null, '23aaasd2424');
INSERT INTO clase7.empresa(id, fecha_creacion, razon_social, representante, ruc)
VALUES (4, '1991-12-31 02.00.00 p.m.', 'silviopd', 'silviopd2', '232424');
INSERT INTO clase7.persona(id, direccion, email, nombre, telefono, empresa_id, id_persona, persona_list_id)
VALUES (1, 'las viñas 149', 'pedi@gmail.com', 'oswal peña diaz', '234982734', null, 0, null);
INSERT INTO clase7.persona(id, direccion, email, nombre, telefono, empresa_id, id_persona, persona_list_id)
VALUES (4, 'las viñas 149', 'pedi@gmail.com', 'oswal peña diaz', '234982734', 1, 0, null);
INSERT INTO clase7.persona(id, direccion, email, nombre, telefono, empresa_id, id_persona, persona_list_id)
VALUES (5, 'las viñas 149', 'pedi@gmail.com', 'oswal peña diaz', '234982734', 1, 0, null);
INSERT INTO clase7.rol(id, nombre)
VALUES (1, 'silviopd2');
INSERT INTO clase7.rol(id, nombre)
VALUES (2, 'oswal');
INSERT INTO clase7.rol(id, nombre)
VALUES (4, 'silviopd2');
INSERT INTO clase7.usuario(id, password, username, empleado_id)
VALUES (1, '232424', 'oswal', null);
INSERT INTO clase7.usuario(id, password, username, empleado_id)
VALUES (2, '232424ss', 'silviopd2', 1);
INSERT INTO clase7.usuario(id, password, username, empleado_id)
VALUES (4, '232424ss', 'silviopd2', 4);