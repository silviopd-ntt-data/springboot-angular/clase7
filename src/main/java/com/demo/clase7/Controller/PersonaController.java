package com.demo.clase7.Controller;

import com.demo.clase7.Dto.PersonaDto;
import com.demo.clase7.Entity.Persona;
import com.demo.clase7.Service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/persona")
public class PersonaController {

    @Autowired
    private IPersonaService personaService;

    @GetMapping("/listar")
    public List<Persona> listar() {
        return personaService.listar();
    }

    @PostMapping("/registrar")
    public Persona crear(@RequestBody Persona persona) {
        return personaService.registrar(persona);
    }

    @PutMapping("/actualizar/{id}")
    public Persona actualizar(@PathVariable("id") Long id, @RequestBody Persona persona) {
        return personaService.modificar(id, persona);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long id) {
        return personaService.eliminar(id);
    }

    @GetMapping("/buscar")
    public List<Persona> buscar(@RequestParam("nombre") String texto) {
        return personaService.buscarPersona(texto);
    }
}
