package com.demo.clase7.Service;

import com.demo.clase7.Dto.PersonaDto;
import com.demo.clase7.Entity.Persona;
import com.demo.clase7.Repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public List<Persona> listar() {
        return personaRepository.findAll();
    }

    @Override
    public Persona registrar(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Persona modificar(Long id, Persona persona) {
        Optional<Persona> p = personaRepository.findById(id);
        if (p.isPresent()) {
            Persona p2 = new Persona();
            p2.setId(id);
            p2.setNombre(persona.getNombre() != null ? persona.getNombre() : p.get().getNombre());
            p2.setEmail(persona.getEmail() != null ? persona.getEmail() : p.get().getEmail());
            p2.setDireccion(persona.getDireccion() != null ? persona.getDireccion() : p.get().getDireccion());
            p2.setTelefono(persona.getTelefono() != null ? persona.getTelefono() : p.get().getTelefono());
            return personaRepository.save(p2);
        }
        return null;
    }

    @Override
    public String eliminar(Long id) {
        Optional<Persona> p = personaRepository.findById(id);
        if (p.isPresent()) {
            personaRepository.deleteById(id);
            return "Eliminado Correctamente";
        }
        return "No se encontro el registro";
    }

    @Override
    public List<Persona> buscarPersona(String texto) {
        return personaRepository.buscarNombre(texto);
    }
}
