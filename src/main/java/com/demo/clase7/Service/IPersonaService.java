package com.demo.clase7.Service;

import com.demo.clase7.Dto.PersonaDto;
import com.demo.clase7.Entity.Persona;

import java.util.List;

public interface IPersonaService {

    //    CRUD
    public List<Persona> listar();

    public Persona registrar(Persona persona);

    public Persona modificar(Long id, Persona persona);

    public String eliminar(Long id);

    public List<Persona> buscarPersona(String texto);
}
