package com.demo.clase7.Service;

import com.demo.clase7.Entity.Rol;

import java.util.List;

public interface IRolService {

    //    CRUD
    public List<Rol> listar();

    public Rol registrar(Rol rol);

    public Rol modificar(Long id, Rol rol);

    public String eliminar(Long id);

}
