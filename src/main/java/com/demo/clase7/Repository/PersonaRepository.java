package com.demo.clase7.Repository;

import com.demo.clase7.Entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {

    @Query(value = "select * from persona where nombre like %?1%", nativeQuery = true)
    public List<Persona> buscarNombre(String nombre);
}
